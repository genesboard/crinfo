<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.dashboard.index', [
            'name' => Auth::user()->name,
            'lastname' => Auth::user()->lastname,
            'id' => Auth::id(),
            'roles' => Auth::user()->roles
        ]);
    }
}
