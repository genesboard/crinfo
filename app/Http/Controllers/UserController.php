<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $users;

    /**
     * UserController constructor.
     * @param UserService $users
     */
    public function __construct(UserService $users)
    {
        $this->users = $users;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('admin.users.index', ['users' => $this->users->show()]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('admin.users.edit', ['user' => $this->users->edit($id), 'roles' => $this->users::rolesButSuperadmin()]);
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request)
    {
        $request->validate();
        $this->users->update($request);
        return redirect()->route('admin_users')->with('status', __('users.form_upd'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->users->delete($id);
        return redirect()->back()->with('status', __('users.form_del'));
    }
}
