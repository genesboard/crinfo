<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User\User;
use Config;
use App\Models\User\Role;

class UserService
{
    /**
     * @return mixed
     */
    public static function show()
    {
        return User::paginate(Config::get('constants.NUM_PAGE'));
    }

    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        $user = User::with('Roles')->find($request->id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->display_name = $request->display_name;
        $user->email = $request->email;
        if ($user->hasRole(User::ROLE_SUPERADMIN)){
            $user->syncRoles($request->role);
            $user->attachRole(User::ROLE_SUPERADMIN);
        } else {
            $user->syncRoles($request->role);
        }
        if($request->file('image'))
        {
            $imageName = time(). '.' . $request->file('image')->getClientOriginalExtension();
            $user->avatar = '/' . Config::get('constants.PATH_USERS_IMG') . $imageName;
            $request->file('image')->move(Config::get('constants.PATH_USERS_IMG'), $imageName);
        }

        $user->status = $request->status;
        $user->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        return User::find($id);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        User::find($id)->delete();
    }

    /**
     * @return array
     */
    public function roles()
    {
        return Role::all()
            ->pluck('display_name', 'id')
            ->toArray();
    }

    public static function rolesButSuperadmin() {
        return Role::where('name', '!=', Role::ROLE_SUPERADMIN)
            ->pluck('display_name', 'id')
            ->toArray();
    }
}