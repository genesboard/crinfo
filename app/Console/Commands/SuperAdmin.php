<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User\User;

class SuperAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:make-superadmin {userEmail} {unmake=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add super admin role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('userEmail'))->first();
        if ($user) {
            if ($user->hasRole($user::ROLE_SUPERADMIN)) {
                if ($this->argument('unmake')) {
                    $user->detachRole($user::ROLE_SUPERADMIN);
                    $this->info("Not super admin anymore.");
                } else {
                    $this->error("The user already is super admin.");
                }
            } else {
                if ($this->argument('unmake')) {
                    $this->error("The user already isn't super admin.");
                } else {
                    $user->attachRole($user::ROLE_SUPERADMIN);
                    $this->info("OK");
                }
            }
        } else {
            $this->line("Who is the user?");
        }
    }
}
