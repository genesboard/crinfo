<?php
namespace App\Helpers\Traits;

/**
 * Trait RequestTrait
 * @package App\Helpers\Traits
 */
trait RequestTrait
{
    /**
     * @param $fileName
     * @param $filePath
     */
    public function fileStore($fileName, $filePath)
    {
        if ($this->hasFile($fileName)) {
            $file = $this->file($fileName);
            $file->store($filePath);
            $this->merge([$fileName => $file->hashName()]);
        }
    }
}
