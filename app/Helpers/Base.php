<?php

/**
 * @return array|false|string
 */
function isProduction()
{
    return getenv("APP_ENV") == 'production';
}

/**
 * @param $icon
 * @return string
 */
function icon($icon)
{
    return asset('/images/icons/' . $icon);
}

/**
 *  Admin assets
 * @param $path
 * @return string
 */
function aa($path)
{
    return asset('/themes/admin/assets/' . $path);
}

/**
 * Admin vendors
 * @param $path
 * @return string
 */
function av($path)
{
    return asset('/themes/admin/vendor/' . $path);
}


/**
 * @param string $place
 * @return mixed|null|string
 */
function getTopBanner($place = 'top')
{
    switch ($place) {
        case 'sidebar':
            $bannerPath = 'images/banners/sidebar';
            break;
        case 'top':
            $bannerPath = 'images/banners/top';
            break;
        default:
            $bannerPath = '';
    }

    $links = [
        'bombadeal' => 'http://bombadeal.ru/cp/5d183602644b8899e1d2b3dac64723/__a35437/__p556',
        'leadbit' => 'http://leadbit.com/?utm_source=conversion&utm_medium=banners&utm_campaign=header',
        'welcome_partner' => 'http://welcome.partners?tracker=171',
        'shakes' => 'http://shakes.pro/?utm_source=conversion.im&utm_medium=banners&utm_campaign=365x400',
        'funcpa' => 'http://funcpa.ru/?utm_source=BANNERS&utm_medium=conversion&utm_campaign=1200&utm_term=action75'
    ];


    $files = File::files(public_path($bannerPath));

    $image = null;

    $link = null;

    $countBanners = count($files);

    if ($countBanners) {

        $inc = intval(cache('banner_number'));

        $number = $inc % $countBanners;
        $image = getenv('APP_URL') . str_replace(public_path(), '', $files[$number]) . "?v=" . $inc;

        $pathParts = pathinfo($files[$number]);

        $link = $links[$pathParts['filename']];
    }

    $link = '<a href="' . $link . '" target="_blank">
                <img src=" ' . $image . '">
            </a>';

    return $link;

}

function getTagIcons($icon = null)
{
    $data = [
        'vk' => [],
        'facebook' => [],
        'instagram' => [],
        'outube' => [],
        'mytarget' => [],
        'yandeks-direkt' => [],
        'google-adwords' => [],
        'seo' => [],
        'e-mail' => [],
        'google-analytics' => [],
        'bombadeal' => [],
        'burzh' => [],
        'gembling' => [],
    ];

    if (empty($icon)) {
        return $data;
    }

    return empty($data[$icon]) ? null : $data[$icon];
}

/**
 * @param $text
 * @return mixed
 */
function getTranslate($text)
{
    $url = 'https://api.multillect.com/translate/json/1.0/629?method=translate/api/translate&from=en&to=ru&text=' . $text . '&sig=1c3258b85b24cc2c390226ebfb0df25d';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = json_decode(curl_exec($ch));
    $translate = $result->result->translated;

    return $translate;
}

/**
 * @param $int
 * @return string
 */
function moneyFormat($int, $round = 2)
{
    return number_format($int, $round, '.', ' ') . ' $';
}

/**
 * @param $int
 */
function indexArrowInt($int)
{
    if ($int > 0) {
        echo '<i class="fa fa-arrow-circle-up text-success"></i>';
    } else {
        echo '<i class="fa fa-arrow-circle-down text-danger"></i>';
    }
}