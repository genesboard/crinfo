<?php

namespace App\Models\User;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    const ROLE_ADMIN = 'admin';
    const ROLE_EDITOR = 'editor';
    const ROLE_USER = 'user';
    const ROLE_SUPERADMIN = 'superadmin';

    /**
     * @return Role
     */
    public static function admin()
    {
        return self::where('name', self::ROLE_ADMIN)->first();
    }

    /**
     * @return Role
     */
    public static function user()
    {
        return self::where('name', self::ROLE_USER)->first();
    }

    /**
     * @return Role
     */
    public static function superadmin()
    {
        return self::where('name', self::ROLE_SUPERADMIN)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
