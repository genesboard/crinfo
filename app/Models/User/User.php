<?php

namespace App\Models\User;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Modules\Crypt\Entities\Portfolio;
use Modules\Post\Entities\Post;

class User extends Authenticatable
{
    use Notifiable, LaratrustUserTrait;

    const ROLE_ADMIN = 'admin';
    const ROLE_EDITOR = 'editor';
    const ROLE_USER = 'user';
    const ROLE_SUPERADMIN = 'superadmin';

    const SOC_GOOGLE = 'google';
    const SOC_FB = 'facebook';
    const SOC_VK = 'vkontakte';

    const REDIRECT_TO = 'admin/dashboard';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts() {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function portfolios() {
        return $this->hasMany(Portfolio::class);
    }

    /**
     * @return float
     */
    public function getDeposit()
    {
        $deposit = \Auth::user()->portfolios()
            ->selectRaw('(coin_val * cost_val) as result')
            ->get()
            ->sum('result');

        return (float) round($deposit, 2);
    }

    /**
     * @return float
     */
    public function getGainLost()
    {
        return (float) $this->portfolios->sum('gain_lost');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'email', 'password', 'provider', 'provider_id', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $userSocial
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public static function SocialAuth($userSocial, $provider) {
        $user = User::where('provider_id', $userSocial->getId())->where('provider', $provider)->first();

        if ($user) {
            \Auth::loginUsingId($user->id);
            return redirect(self::REDIRECT_TO);
        } else {
            $name = '';
            $lastname = '';
            if ($provider == self::SOC_GOOGLE) {
                $name = $userSocial->user['name']['givenName'];
                $lastname = $userSocial->user['name']['familyName'];
            }
            if ($provider == self::SOC_FB) {
                $name = $userSocial->getName();
            }
            if ($provider == self::SOC_VK) {
                $name = $userSocial->user['first_name'];
                $lastname = $userSocial->user['last_name'];
            }

            if ($userSocial->getEmail()) {
                $account = User::where('email', $userSocial->getEmail())->first();
                if($account) {
                    $account->provider = $provider;
                    $account->provider_id = $userSocial->getId();
                    $account->save();
                    \Auth::loginUsingId($account->id);
                    return redirect(self::REDIRECT_TO);
                } else {
                    $user = User::create([
                        'name' => $name,
                        'lastname' => $lastname,
                        'email' => $userSocial->getEmail(),
                        'provider' => $provider,
                        'provider_id' => $userSocial->getId(),
                        'avatar' => $userSocial->getAvatar()
                    ]);
                    $user->attachRole(self::ROLE_USER);

                    \Auth::loginUsingId($user->id);
                    return redirect(self::REDIRECT_TO);
                }
            } else {
                $user = User::create([
                    'name' => $name,
                    'lastname' => $lastname,
                    'email' => $userSocial->getId(),
                    'provider' => $provider,
                    'provider_id' => $userSocial->getId(),
                    'avatar' => $userSocial->getAvatar()
                ]);
                $user->attachRole(self::ROLE_USER);

                \Auth::loginUsingId($user->id);
                return redirect(self::REDIRECT_TO);
            }
        }

    }
}
