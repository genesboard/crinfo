<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence; // base trait
use Sofa\Eloquence\Mappable; // extension trait


/**
 * Class User
 * @package App\Models
 * @property string $login
 * @property string $name
 */
abstract class Base extends Model
{
    use Eloquence;
    use Mappable;

    /**
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * @param $data
     * @return Base
     */
    public static function upsert($data): Base
    {
        if (!empty($data['id'])) {
            $model = self::find($data['id']);
            $model->update($data);
        } else {
            $model = self::create($data);
        }

        return $model;
    }
}