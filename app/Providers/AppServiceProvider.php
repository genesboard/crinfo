<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Form::component('adminText', 'admin.components.form.text', ['name', 'label', 'lbSize' => 3, 'ctrSize' => 8]);
        Form::component('adminColor', 'admin.components.form.color', ['name', 'label', 'lbSize' => 3, 'ctrSize' => 8]);
        Form::component('adminTextArea', 'admin.components.form.textarea', ['name', 'label', 'lbSize' => 3, 'ctrSize' => 8]);
        Form::component('adminFile', 'admin.components.form.file', ['name', 'label', 'lbSize' => 3, 'ctrSize' => 8, 'filePath']);
        Form::component('adminSubmit', 'admin.components.form.submit', []);
        Form::component('adminActions', 'admin.components.form.actions', ['urlEdit', 'urlDestroy']);
        Form::component('adminSelect', 'admin.components.form.select', ['name', 'label', 'data' => [], 'lbSize' => 3, 'ctrSize' => 8, 'multi' => false]);
        Form::component('adminCheckBox', 'admin.components.form.checkbox', ['name', 'label', 'lbSize' => 3, 'ctrSize' => 8]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
