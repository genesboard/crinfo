@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('users') }}
@endsection
@section('content')
<div class="tray tray-center">
    <div class="col-sm-12">
        @if(session('status'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="fa fa-check pr10"></i>
            {{ session('status') }}
        </div>
        @endif
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <span class="panel-icon"><i class="fa fa-list"></i></span>
                        <span class="panel-title">{{ __('users.list') }}</span>
                    </div>
                </div>
            </div>

            @isset($users)
            <div class="panel-body pn">
                <div class="table-responsive">
                    <table class="table admin-form table-condensed table-responsive table-striped table-bordered">
                        <thead>
                        <tr class="bg-light">
                            <th class="text-center">{{ __('users.id') }}</th>
                            <th class="text-center">{{ __('users.avatar') }}</th>
                            <th class="text-center">{{ __('users.name') }}</th>
                            <th class="text-center">{{ __('users.lastname') }}</th>
                            <th class="text-center">{{ __('users.display_name') }}</th>
                            <th class="text-center">{{ __('users.email') }}</th>
                            <th class="text-center">{{ __('users.status') }}</th>
                            <th class="text-center">{{ __('users.role') }}</th>
                            <th class="text-center">{{ __('users.action') }}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($users as $user)
                        <tr>
                            <td class="text-center">{{ $user->id }}</td>
                            <td class="text-center">
                                @if($user->avatar)
                                <img style="max-width: 40px; border: 2px solid #888; border-radius: 50%;" src="{{$user->avatar }}" class="img-responsive">
                                @endif
                            </td>
                            <td class="text-center">{{ $user->name }}</td>
                            <td class="text-center">{{ $user->lastname }}</td>
                            <td class="text-center">{{ $user->display_name }}</td>
                            <td class="text-center">{{ $user->email }}</td>
                            <td class="text-center">{{ $user->status ? __('users.form_enable') : __('users.form_disable') }}</td>
                            <td class="text-center">
                                <ul class="list-unstyled">
                                @foreach ($user->roles as $role)
                                    <li>{{ $role->display_name }}</li>
                                @endforeach
                                </ul>
                            </td>
                            <td class="text-right">
                                {{ Form::open(['method' => 'delete', 'route' => ['admin_delete_user', $user->id],]) }}
                                <a href="{{route('admin_update_user') . '/' . $user->id}}">
                                    {{Form::button('<i class="fa fa-edit"></i>', ['class' => 'btn btn-xs btn-warning'])}}
                                </a>
                                {{ Form::hidden('id', $user->id) }}
                                <button type="submit" class="btn btn-xs btn-danger confirm" data-confirm="{{__('validation.confirm_delete')}}"><i class="fa fa-times"></i></button>
                                {{ Form::close() }}
                            </td>
                        </tr>

                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        <nav>{{ $users->links() }}</nav>
                    </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
@stop
