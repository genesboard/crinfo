@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('user-edit', $user) }}
@endsection
@section('content')
    <div class="admin-form">
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable col-sm-8 col-sm-offset-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="fa fa-warning pr10"></i>
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <div class="panel panel-dark heading-border col-sm-8 col-sm-offset-2">
            <div class="panel-heading">
                <span class="panel-title">{{ __('users.edit_user') }}</span>
            </div>
            {{Form::model($user, ['url' => route('admin_update_user'), 'files' => true, 'class' => 'horizontal'])}}
            <div class="panel-body">
                {{ Form::hidden('id', $user->id) }}
                <div class="section row mt30">
                    {{Form::label('image', __('users.avatar'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="fileupload fileupload-new col-sm-4" data-provides="fileupload" data-name="icon">
                        <div class="fileupload-preview thumbnail cursor mb15">
                            <img src="{{$user->avatar}}" title="placeholder img">
                        </div>
                        <span class="btn btn-dark btn-file btn-block btn-xs">
                        <span class="fileupload-new">{{__('category::admin.change')}}</span>
                        <span class="fileupload-exists">{{__('category::admin.change')}}</span>
                            {{Form::file('image', ['class' => 'image'])}}
                    </span>
                    </div>
                </div>


                <div class="section row mt30">
                    {{Form::label('name', __('users.name'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label for="names" class="field prepend-icon">
                            {{ Form::text('name', $user->name, ['class' => 'gui-input', 'id' => 'name', 'placeholder' => __('users.name')]) }}
                            <label for="names" class="field-icon"><i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('lastname', __('users.lastname'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label for="names" class="field prepend-icon">
                            {{ Form::text('lastname', $user->lastname, ['class' => 'gui-input', 'id' => 'lastname', 'placeholder' => __('users.lastname')]) }}
                            <label for="names" class="field-icon"><i class="fa fa-group"></i>
                            </label>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('display_name', __('users.display_name'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label for="names" class="field prepend-icon">
                            {{ Form::text('display_name', $user->display_name, ['class' => 'gui-input', 'id' => 'display_name', 'placeholder' => __('users.display_name')]) }}
                            <label for="names" class="field-icon"><i class="fa fa-meh-o"></i>
                            </label>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('email', __('users.email'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label for="names" class="field prepend-icon">
                            {{ Form::text('email', $user->email, ['class' => 'gui-input', 'id' => 'email', 'placeholder' => __('users.email')]) }}
                            <label for="names" class="field-icon"><i class="fa fa-envelope-o"></i>
                            </label>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('status', __('users.status'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label class="field select">
                            {{ Form::select('status', ['1' => __('users.form_enable'), '0' => __('users.form_disable')], isset($user) ? $user->status : old('status'), ['id' => 'status']) }}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('role', __('users.role'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        {{ Form::select('role[]', $roles, $user->roles, ['class' => 'form-control', 'id' => 'role', 'multiple' => 'multiple']) }}
                    </div>
                </div>

                {{ Form::submit(__('users.form_save'), ['class' => 'btn btn-xs btn-success pull-right']) }}
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('add_script')
    <script>
        //Select2
        $('#role').select2({
            placeholder: '{{ __('users.select_role') }}'
        });
    </script>
@endsection
