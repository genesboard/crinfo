@extends('admin.layouts.blank')

@section('content')
    <div class="admin-form theme-info" id="login1" style="width: 30% !important;">
        <div class="row mb15 table-layout">
            <div class="col-xs-6 va-m pln">
                <a href="/" title="Вернуться на сайт">
                    <span style="color: white; font-size: 15px; font-weight: bold">{{ config('app.name') }}</span>
                </a>
            </div>
            
            <div class="col-xs-6 text-right va-b pr5">
                <div class="login-links">
                    <a href="{{ route('login') }}" class="active" title="Sign In">{{ __('auth.form.sign-in') }}</a>
                    <span class="text-white"> | </span>
                    <a href="{{ route('register') }}" class="" title="Register">{{ __('auth.form.register') }}</a>
                </div>
            </div>
        </div>
        
        <div class="panel panel-info mt10 br-n">
            <!-- end .form-header section -->
            {{ Form::open(['method' => 'post', 'route' => 'login', 'id' => 'login']) }}
                {{Form::token()}}
                <div class="panel-body bg-light p30">
                    <div class="row">
                        <div class="pr30">
                            <div class="section">
                                {{Form::label('email', 'E-Mail', ['class' => 'field-label text-muted fs18 mb10'])}}
                                <label for="email" class="field prepend-icon">
                                    {{Form::email('email', old('email'), ['id' => 'email', 'class' => 'form-control', 'placeholder' => __('auth.form.enter-email'), 'autofocus'])}}
                                   <label for="username" class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </label>
                                </label>
                            </div>
                            
                            <!-- end section -->
                            <div class="section">
                                {{Form::label('username', __('auth.form.password'), ['class' => 'field-label text-muted fs18 mb10'])}}
                                <label for="password" class="field prepend-icon">
                                    {{Form::password('password', ['id' => 'password', 'class' => 'gui-input', 'placeholder' => __('auth.form.enter-password')])}}
                                    <label for="password" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </label>
                                </label>
                            </div>
                            <!-- end section -->
                        
                        </div>
                    </div>
                </div>

                @if($errors->has('email') || $errors->has('password'))
                    <div class="alert alert-danger">
                        {{ __('auth.failed') }}
                    </div>
                @endif

            <!-- end .form-body section -->
                <div class="panel-footer clearfix p10 ph15">
                    {{Form::submit(__('auth.form.sign-in'), ['class' => 'button btn-primary mr10 pull-right'])}}
                    <label class="switch ib switch-primary pull-left input-align mt10">
                        {{Form::checkbox('remember', null, true, ['id' => 'remember'])}}
                        <label for="remember" data-on="{{ __('auth.form.yep') }}" data-off="{{ __('auth.form.nay') }}"></label>
                        <span>{{ __('auth.form.remember') }}</span>
                    </label>
                </div>
                <!-- end .form-footer section -->
            {{Form::close()}}
        </div>
    </div>
@endsection
