@extends('admin.layouts.blank')

@section('content')
    <div class="admin-form theme-info mw700" style="margin-top: 3%; width: 50% !important;" id="login1">
        <div class="row mb15 table-layout">
            <div class="col-xs-6 va-m pln">
                <a href="/" title="Return to Dashboard">
                    <span style="color: white; font-size: 15px; font-weight: bold">{{ config('app.name') }}</span>
                </a>
            </div>
    
            <div class="col-xs-6 text-right va-b pr5">
                <div class="login-links">
                    <a href="{{ route('login') }}" class="" title="{{ __('auth.form.sign-in') }}">{{ __('auth.form.sign-in') }}</a>
                    <span class="text-white"> | </span>
                    <a href="{{ route('register') }}" class="active" title="{{ __('auth.form.register') }}">{{ __('auth.form.register') }}</a>
                </div>
            </div>
        </div>
        
        <div class="panel panel-info mt10 br-n">
            {{ Form::open(['method' => 'post', 'route' => 'register', 'id' => 'account2']) }}
                {{Form::token()}}
                <div class="panel-body p25 bg-light">
                    <div class="section-divider mt10 mb40">
                        <span>{{ __('auth.form.enter-details') }}</span>
                    </div>
                    <!-- .section-divider -->
                    
                    <div class="section row">
                        <div class="col-md-6">
                            <label for="name" class="field prepend-icon">
                                {{Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => __('auth.form.name'), 'autofocus'])}}
                                <label for="firstname" class="field-icon">
                                    <i class="fa fa-user"></i>
                                </label>
                            </label>
                        </div>
                        <!-- end section -->
                        
                        <div class="col-md-6">
                            <label for="lastname" class="field prepend-icon">
                                {{Form::text('lastname', old('lastname'), ['class' => 'form-control', 'id' => 'lastname', 'placeholder' => __('auth.form.last-name')])}}
                                <label for="lastname" class="field-icon">
                                    <i class="fa fa-user"></i>
                                </label>
                            </label>
                        </div>
                        <!-- end section -->
                    </div>
                    <!-- end .section row section -->
                    
                    <div class="section">
                        <label for="email" class="field prepend-icon">
                            {{Form::email('email', old('email'), ['id' => 'email', 'class' => 'gui-input', 'placeholder' => __('auth.form.enter-email')])}}
                            <label for="email" class="field-icon">
                                <i class="fa fa-envelope"></i>
                            </label>
                        </label>
                    </div>
                    <!-- end section -->
                    
                    <div class="section">
                        <label for="password" class="field prepend-icon">
                            {{Form::password('password', ['id' => 'password', 'class' => 'gui-input', 'placeholder' => __('auth.form.password')])}}
                            <label for="password" class="field-icon">
                                <i class="fa fa-unlock-alt"></i>
                            </label>
                        </label>
                    </div>
                    <!-- end section -->
                    
                    <div class="section">
                        <label for="confirmPassword" class="field prepend-icon">
                            {{Form::password('password_confirmation', ['id' => 'confirmPassword', 'class' => 'gui-input', 'placeholder' => __('auth.form.pass-confirm')])}}
                            <label for="confirmPassword" class="field-icon">
                                <i class="fa fa-lock"></i>
                            </label>
                        </label>
                    </div>

                    @if (count($errors->all()))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{$error}}</div>
                            @endforeach
                        </div>
                    @endif
                </div>

                <!-- end .form-body section -->
                <div class="panel-footer clearfix">
                    {{Form::submit(__('auth.form.sign-up'), ['class' => 'button btn-primary pull-right'])}}
                </div>
                <!-- end .form-footer section -->
            {{Form::close()}}
        </div>
    </div>
@endsection
