<!-- Start: Header -->
<header class="navbar navbar-fixed-top navbar-shadow">
    <div class="navbar-branding">
        <a class="navbar-brand" href="/">
            <b>{{ config('app.name') }}</b>
        </a>
        <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
    </div>

    <div class="mt20 navbar-left">
        <a target="_blank" href="https://www.bestchange.ru/" onclick="this.href='https://www.bestchange.ru/?p=751275'" class="label label-success">Обмен крипты</a>
        <a target="_blank" href="https://ww-pay.com/" onclick="this.href='https://ww-pay.com/?partner=67784'" class="label label-primary">Хороший обменник</a>
        <a target="_blank" href="http://advcash.com/" onclick="this.href='http://wallet.advcash.com/referral/99bf98b6-c210-46d2-a2e6-e3ad212629be'" class="label label-warning">Оффшор карты</a>

    </div>
    
    <div class="mt20 navbar-right">
        <span class="label label-success">Депозит</span>
        <span class="label label-default">{{ Auth::user()->getDeposit() }} $</span>

        <span class="label label-warning">Баланс</span>
        <span class="label label-default mr5">{{ Auth::user()->getGainLost() }} $</span>
    </div>
</header>
<!-- End: Header -->
