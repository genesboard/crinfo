<!-- Start: Topbar-Dropdown -->
<div id="topbar-dropmenu" class="alt">
    <div class="topbar-menu row">
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-primary light">
                <span class="glyphicon glyphicon-inbox text-muted"></span>
                <span class="metro-title">Messages</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-info light">
                <span class="glyphicon glyphicon-user text-muted"></span>
                <span class="metro-title">Users</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-success light">
                <span class="glyphicon glyphicon-headphones text-muted"></span>
                <span class="metro-title">Support</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-system light">
                <span class="glyphicon glyphicon-facetime-video text-muted"></span>
                <span class="metro-title">Videos</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-warning light">
                <span class="fa fa-gears text-muted"></span>
                <span class="metro-title">Settings</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="metro-tile bg-alert light">
                <span class="glyphicon glyphicon-picture text-muted"></span>
                <span class="metro-title">Pictures</span>
            </a>
        </div>
    </div>
</div>
<!-- End: Topbar-Dropdown -->

<!-- Start: Topbar -->
<header id="topbar" class="alt">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-icon crumb-link">
                <i class="glyphicon glyphicon-home"></i>
            </li>
        </ol>
        @yield('crumb')

    </div>
    <div class="topbar-right">
        <div class="ib topbar-dropdown">
            <label for="topbar-multiple" class="control-label pr10 fs11 text-muted"></label>
            <select id="topbar-multiple" class="hidden">
                <optgroup label="Filter By:">
                    <option value="1-1">Last 30 Days</option>
                    <option value="1-2" selected="selected">Last 60 Days</option>
                    <option value="1-3">Last Year</option>
                </optgroup>
            </select>
        </div>
        <div class="ml15 ib va-m" id="toggle_sidemenu_r">
            <a href="#" class="pl5">
                <i class="fa fa-sign-in fs22 text-primary"></i>
                <span class="badge badge-danger badge-hero">3</span>
            </a>
        </div>
    </div>
</header>
<!-- End: Topbar -->
