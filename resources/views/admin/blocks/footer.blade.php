<!-- Begin: Page Footer -->
<footer id="content-footer" class="affix">
    <div class="row">
        <div class="col-md-2">
            <span class="footer-legal">© {{ date('Y') }} {{ config('app.name') }}</span>
        </div>
        <div class="col-md-10 text-right">
        @if (Auth::user()->portfolios)
            <span class="footer-meta">
            @foreach (Auth::user()->portfolios as $p)
                <span class="label label-primary">{{ $p->coin }}</span>
                <span class="label label-default mr5">{{ $p->rate_now }}</span>
            @endforeach
            </span>
        @endif

            <a href="#content" class="footer-return-top">
                <span class="fa fa-arrow-up"></span>
            </a>
        </div>
    </div>
</footer>
<!-- End: Page Footer -->
