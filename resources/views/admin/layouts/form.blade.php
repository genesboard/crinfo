@extends('admin.layouts.app')

@section('content')

<div class="row">

    <div class="col-sm-10 col-sm-offset-1 admin-form theme-primary light">

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="fa fa-warning pr10"></i>
                    {{ $error }}
                </div>
            @endforeach
        @endif

        <div class="panel heading-border panel-primary light">
            <div class="panel-body bg-light">
                @yield('content_form')
            </div>
        </div>
    </div>
</div>
@endsection
