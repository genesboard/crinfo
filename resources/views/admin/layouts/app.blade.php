<!DOCTYPE html>
<html>

<head>
    
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>Crypt Inform</title>
    <meta name="description" content="panel">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ aa('skin/default_skin/css/theme.css') }}">
    
    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="{{ aa('admin-tools/admin-forms/css/admin-forms.min.css') }}">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    {{--Summernote
    <link href="{{av('plugins/summernote/summernote.css')}}" rel="stylesheet" type="text/css">
    <link href="{{av('plugins/summernote/summernote-bs3.css')}}" rel="stylesheet" type="text/css">
    --}}
    {{-- Select2
    <link href="{{ av('plugins/select2/css/core.css') }}" rel="stylesheet" type="text/css">
    --}}
    <link rel="stylesheet" href="/css/admin.css" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    {{--<link rel="stylesheet/less" type="text/css" href="assets/skins/default_skin/less/theme.less" />--}}
    {{--<script src="/js/less.min.js" type="text/javascript"></script>--}}
    
    @yield('add_head')

</head>

<body class="@section('body_class')dashboard-page @show">

<!-------------------------------------------------------------+
  <body> Helper Classes:
---------------------------------------------------------------+
  '.sb-l-o' - Sets Left Sidebar to "open"
  '.sb-l-m' - Sets Left Sidebar to "minified"
  '.sb-l-c' - Sets Left Sidebar to "closed"

  '.sb-r-o' - Sets Right Sidebar to "open"
  '.sb-r-c' - Sets Right Sidebar to "closed"
---------------------------------------------------------------+
 Example: <body class="example-page sb-l-o sb-r-c">
 Results: Sidebar left Open, Sidebar right Closed
--------------------------------------------------------------->

<!-- Start: Main -->
<div id="main">
    @include('admin.blocks.header')
    @include('admin.blocks.left_sidebar')

    <section id="content_wrapper">
    {{--
        @include('admin.blocks.top_bar')
    --}}
        <section id="content" class="table-layout animated fadeIn">
            @yield('content')
        </section>

        @include('admin.blocks.footer')
        {{--
        @include('admin.blocks.right_sidebar')
        --}}
    </section>
</div>

@include('admin.blocks.scripts')

</body>
</html>
