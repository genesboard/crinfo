@isset($image)
<div class="section mt50">
    <div class="col-sm-6 col-sm-offset-4">
        <img src="{{$image}}" class="imageMain">
    </div>
</div>
@endisset

<div class="section row mt50">
    {{Form::label('file', $title, ['class' => 'field-label col-sm-3'])}}
    <div class="col-sm-5">
        <label class="field prepend-icon append-button file">
            <input type="file" class="gui-file" name="file" id="file-input" onChange="document.getElementById('file-input-text').value = this.value;">
            <input type="text" class="gui-input" id="file-input-text">
            <label class="field-icon"><i class="fa fa-upload"></i>
            </label>
        </label>
    </div>
</div>

<div class="section row mt20">
    <div class="box-2 col-sm-6">
        <div class="result"></div>
    </div>
    <div class="box-2 img-result hide col-sm-6">
        <img class="cropped" src="" alt="">
    </div>
</div>

<div class="section row mt20">
    <div class="col-sm-8 options hide">
        <input id="namberInput" type="number" class="img-w form-control" value="300" min="100" max="1200" />
    </div>
    <div class="col-sm-2 btn-group">
        <a class="btn btn-dark btn-xs save pull-right hide">{{__('post::admin.crop')}}</a>
        <a href="" class="btn btn-dark btn-xs download pull-right hide">{{__('post::admin.download')}}</a>
    </div>
</div>

{{Form::text($fieldName, null, ['class' => 'hidden', 'id' => 'croppedInput'])}}

<style>
    img {
        max-width: 100%;
    }
    .imageMain {
        margin-bottom: 25px;
    }
</style>
<script>
    'use strict';

    // vars
    var result = document.querySelector('.result'),
        img_result = document.querySelector('.img-result'),
        img_w = document.querySelector('.img-w'),
        img_h = document.querySelector('.img-h'),
        options = document.querySelector('.options'),
        save = document.querySelector('.save'),
        cropped = document.querySelector('.cropped'),
        dwn = document.querySelector('.download'),
        upload = document.querySelector('#file-input'),
        hiddenInput = document.querySelector('#croppedInput'),
        cropper = '';

    // on change show image with crop options
    upload.addEventListener('change', function (e) {
        if (e.target.files.length) {
            // start file reader
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.target.result) {
                    // create new image
                    var img = document.createElement('img');
                    img.id = 'image';
                    img.src = e.target.result;
                    // clean result before
                    result.innerHTML = '';
                    // append new image
                    result.appendChild(img);
                    // show save btn and options
                    save.classList.remove('hide');
                    options.classList.remove('hide');
                    // init cropper
                    cropper = new Cropper(img);
                }
            };
            reader.readAsDataURL(e.target.files[0]);
        }
    });

    // save on click
    save.addEventListener('click', function (e) {
        e.preventDefault();
        // get result to data uri
        var imgSrc = cropper.getCroppedCanvas({
            width: img_w.value // input value
        }).toDataURL();
        // remove hide class of img
        cropped.classList.remove('hide');
        img_result.classList.remove('hide');
        // show image cropped
        cropped.src = imgSrc;
        dwn.classList.remove('hide');
        dwn.download = 'imagename.png';
        dwn.setAttribute('href', imgSrc);
        hiddenInput.setAttribute('value', imgSrc);

    });
</script>