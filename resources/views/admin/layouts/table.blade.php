@extends('admin.layouts.app')

@section('content')
    <div class="panel">
        <div class="panel-heading panel-visible">
            <span class="panel-title">@yield('title')</span>
            <div class="widget-menu pull-right mr10">
                <a href="@yield('routeButton')" class="btn btn-sm btn-success">
                    <span class="fa fa-plus"></span> @yield('titleButton')
                </a>
            </div>
        </div>

        <div class="panel-body">
            <div class="bs-component">
                @yield('content_table')
            </div>
        </div>
    </div>
@endsection
