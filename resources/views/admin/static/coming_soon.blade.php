@extends('admin.layouts.blank')

@section('content')
            <div class="admin-form theme-info" id="login1" style="margin-top: 6%;">
                
                <div id="counter"></div>
                
                <h1 class="coming-soon-title">Все будет совсем скоро!</h1>
                <div class="panel panel-info bw10">
                    
                    <!-- end .form-header section -->
                    <form method="post" action="/" id="contact">
                        <div class="panel-menu">
                            <div class="row">
                                <div class="col-md-9">
                                    <label for="password" class="field prepend-icon">
                                        <input type="text" name="password" id="password" class="gui-input" placeholder="Ваш Email адрес">
                                        <label for="password" class="field-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </label>
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="button btn-info mr10 btn-block">Подписаться</button>
                                </div>
                            </div>
                        </div>
                        <!-- end .form-body section -->
                    
                    </form>
                </div>
            </div>
@endsection