{{ Form::label($name, $label, ['class' => 'col-sm-'. $lbSize .' control-label']) }}
<div class="col-sm-{{ $ctrSize }}">
    <label class="block mt5 switch switch-primary">
        {{ Form::hidden($name, 0) }}
        {{ Form::checkbox($name, 1, null, ['id' => 'chkbox_' . $name]) }}
        <label for="chkbox_{{$name}}" data-on="{{ __('index.yes') }}" data-off="{{ __('index.no') }}"></label>
    </label>
</div>



