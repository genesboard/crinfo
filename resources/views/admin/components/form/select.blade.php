{{ Form::label($name, $label, ['class' => 'col-sm-'. $lbSize .' control-label']) }}

<div class="col-sm-{{ $ctrSize }}">
    {{ Form::select($name, $data, old($name), ['class' => 'select2 form-control', 'multiple' => $multi]) }}
</div>