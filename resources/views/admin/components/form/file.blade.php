{{ Form::label($name, $label, ['class' => 'col-sm-'.$lbSize.' control-label']) }}
<div class="col-sm-{{ $ctrSize }}">
    <label class="field prepend-icon append-button file">
        {!! Form::file($name, ['class' => 'gui-file', 'onchange' => "document.getElementById('placeholder_".$name."').value = this.value;"]) !!}
        <input type="text" class="gui-input" id="placeholder_{{ $name }}" placeholder="">
        <label class="field-icon">
            <i class="fa fa-upload"></i>
        </label>
    </label>
</div>
<div class="col-sm-1">
    <img height="37px" src="{{ $filePath }}"/>
</div>