{{ Form::open(['url' => $urlDestroy ]) }}
    {{ Form::hidden('_method', 'DELETE') }}
    <a href="{{ $urlEdit }}" class="btn btn-warning dark btn-xs"><i class="fa fa-edit"></i></a>
    {{ Form::button('<i class="fa fa-remove"></i>',['type' => 'submit', 'class' => 'btn btn-danger dark btn-xs'])}}
{{ Form::close() }}

