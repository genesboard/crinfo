{{ Form::label($name, $label, ['class' => 'col-sm-'. $lbSize .' control-label']) }}
<div class="col-sm-{{ $ctrSize }}">
    {{ Form::text($name, old($name), ['class' => 'form-control']) }}
</div>

