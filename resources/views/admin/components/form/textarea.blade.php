<div class="form-group mt30">
    {{ Form::label($name, $label, ['class' => 'col-sm-'. $lbSize .' control-label']) }}
    <div class="col-sm-{{ $ctrSize }}">
        <div class="bs-component">
            {{ Form::textarea($name, old($name), ['class' => 'form-control gui-textarea summernote']) }}
        </div>
    </div>
</div>