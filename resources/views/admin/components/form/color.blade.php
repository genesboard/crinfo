{{ Form::label($name, $label, ['class' => 'col-sm-'. $lbSize .' control-label']) }}

<div class="col-sm-{{ $ctrSize }}">
    <div class="input-group colorpicker-component cursor colorpicker-element">
            <span class="input-group-addon">
              <i style="background-color: rgb(255,255,255);"></i>
            </span>
        {{ Form::text($name, old($name), ['class' => 'form-control']) }}
    </div>
</div>