<?php

return [
    'dashboard' => 'Dashboard',
    'categories' => 'Categories',
    'category-types' => 'Category types',
    'category-create' => 'Create category',
    'category-type-create' => 'Create category type',
    'posts' => 'Posts',
    'post-types' => 'Post types',
    'post-create' => 'Create post',
    'post-type-create' => 'Create post type',
    'users' => 'Users',
    'banners' => 'Banners',
    'banner-types' => 'Banner types',
    'banner-create' => 'Create banner',
    'banner-type-create' => 'Create banner type'
];