<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'form' => [
        'password' => 'Password',
        'enter-password' => 'Enter password',
        'enter-email' => 'Enter e-mail',
        'enter-details' => 'Enter your details',
        'remember' => 'Remember me',
        'yep' => 'yes',
        'nay' => 'no',
        'sign-in' => 'Sign In',
        'register' => 'Register',
        'name' => 'First name',
        'last-name' => 'Last name',
        'email-address' => 'Email address',
        'pass-confirm' => 'Confirm  password',
        'sign-in' => 'Sign in',
        'sign-up' => 'Sign up'
    ],

];
