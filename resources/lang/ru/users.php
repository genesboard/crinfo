<?php

return [
    'users' => 'Пользователи',
    'id' => '№',
    'name' => 'Имя',
    'lastname' => 'Фамилия',
    'display_name' => 'Никнейм',
    'email' => 'Почта',
    'avatar' => 'Аватар',
    'status' => 'Статус',
    'role' => 'Роль',
    'list' => 'Пользователи',
    'action' => 'Действие',
    'edit' => 'Изменить',
    'delete' => 'Удалить',
    'edit_user' => 'Редактировать',
    'avatar_browse' => 'Выбрать аватар',
    'img_inp' => 'Добавьте аватар',
    'form_save' => 'Сохранить',
    'form_enable' => 'Включен',
    'form_disable' => 'Отключен',
    'form_upd' => 'Данные сохранены',
    'form_del' => 'Пользователь удален',
    'role_admin' => 'Администратор',
    'role_writer' => 'Редактор',
    'role_user' => 'Пользователь',
    'select_role' => 'Выберите роль',
];