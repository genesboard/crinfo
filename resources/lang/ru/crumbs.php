<?php

return [
    'dashboard' => 'Рабочий стол',
    'categories' => 'Категории',
    'category-types' => 'Типы категорий',
    'category-create' => 'Создание категории',
    'category-type-create' => 'Создание типа категории',
    'posts' => 'Статьи',
    'post-types' => 'Типы статей',
    'post-create' => 'Создание статьи',
    'post-type-create' => 'Создание типа статьи',
    'users' => 'Пользователи',
    'banners' => 'Баннеры',
    'banner-types' => 'Типы баннеров',
    'banner-create' => 'Создание баннера',
    'banner-type-create' => 'Создание типа баннера'
];