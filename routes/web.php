<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'auth'], function() {
    Auth::routes();
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

    Route::get('/social_login/{provider}', '\App\Http\Controllers\Auth\LoginController@redirectToProvider')->name('social_login');
    Route::get('/social_login/callback/{provider}', '\App\Http\Controllers\Auth\LoginController@handleProviderCallback');
});

Route::group(['middleware' => ['web', 'auth']], function() {
    Route::get('/coming_soon', ['as' => 'static.coming_soon', 'uses' => 'IndexController@comingSoon']);
});

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth', 'role:superadmin|admin']], function() {
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

    Route::get('/users', ['uses' => 'UserController@show', 'as' => 'admin_users']);
    Route::get('/update/{id}', ['uses' => 'UserController@edit', 'as' => 'admin.user.edit']);
    Route::post('/update/', ['uses' => 'UserController@update', 'as' => 'admin_update_user']);
    Route::delete('/delete/{id?}', ['uses' => 'UserController@destroy', 'as' => 'admin_delete_user']);
});