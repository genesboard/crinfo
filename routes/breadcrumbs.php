<?php

Breadcrumbs::register('admin.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('crumbs.dashboard'), route('admin.dashboard'));
});

Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('crumbs.dashboard'), route('admin.dashboard'));
});

Breadcrumbs::register('categories', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.categories'), route('category.categories.list'));
});

Breadcrumbs::register('category-types', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.category-types'), route('category.types.list'));
});

Breadcrumbs::register('category-create', function ($breadcrumbs) {
    $breadcrumbs->parent('categories');
    $breadcrumbs->push(__('crumbs.category-create'), route('category.categories.create'));
});

Breadcrumbs::register('category-type-create', function ($breadcrumbs) {
    $breadcrumbs->parent('category-types');
    $breadcrumbs->push(__('crumbs.category-type-create'), route('category.types.create'));
});

Breadcrumbs::register('category-edit', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('categories');
    $breadcrumbs->push($category->name, route('category.categories.edit', $category->id));
});

Breadcrumbs::register('category-type-edit', function ($breadcrumbs, $type) {
    $breadcrumbs->parent('category-types');
    $breadcrumbs->push($type->name, route('category.types.edit', $type->id));
});


Breadcrumbs::register('posts', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.posts'), route('post.posts.list'));
});

Breadcrumbs::register('post-types', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.post-types'), route('post.types.list'));
});

Breadcrumbs::register('post-create', function ($breadcrumbs) {
    $breadcrumbs->parent('posts');
    $breadcrumbs->push(__('crumbs.post-create'), route('post.posts.create'));
});

Breadcrumbs::register('post-type-create', function ($breadcrumbs) {
    $breadcrumbs->parent('post-types');
    $breadcrumbs->push(__('crumbs.post-type-create'), route('post.types.create'));
});

Breadcrumbs::register('post-edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('posts');
    $breadcrumbs->push($post->title, route('post.posts.edit', $post->id));
});

Breadcrumbs::register('post-type-edit', function ($breadcrumbs, $type) {
    $breadcrumbs->parent('post-types');
    $breadcrumbs->push($type->name, route('post.types.edit', $type->id));
});


Breadcrumbs::register('users', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.users'), route('admin_users'));
});

Breadcrumbs::register('user-edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('users');
    $breadcrumbs->push($user->name . ' ' . $user->lastname, route('admin.user.edit', $user->id));
});


Breadcrumbs::register('banners', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.banners'), route('banners'));
});

Breadcrumbs::register('banner-types', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(__('crumbs.banner-types'), route('banners_types'));
});

Breadcrumbs::register('banner-create', function ($breadcrumbs) {
    $breadcrumbs->parent('banners');
    $breadcrumbs->push(__('crumbs.banner-create'), route('post.posts.create'));
});

Breadcrumbs::register('banner-type-create', function ($breadcrumbs) {
    $breadcrumbs->parent('banner-types');
    $breadcrumbs->push(__('crumbs.banner-type-create'), route('post.types.create'));
});

Breadcrumbs::register('banner-edit', function ($breadcrumbs, $banner) {
    $breadcrumbs->parent('banners');
    $breadcrumbs->push($banner->name, route('post.posts.edit', $banner->id));
});

Breadcrumbs::register('banner-type-edit', function ($breadcrumbs, $type) {
    $breadcrumbs->parent('banner-types');
    $breadcrumbs->push($type->name, route('post.types.edit', $type->id));
});
