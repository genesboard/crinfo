<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\User\Role;

class AddNewRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = new Role();
        $role->name         = Role::ROLE_ADMIN;
        $role->display_name = 'Администратор'; // optional
        $role->description  = 'Бог и владыка сего творения'; // optional
        $role->save();

        $role = new Role();
        $role->name         = Role::ROLE_EDITOR;
        $role->display_name = 'Редактор'; // optional
        $role->description  = 'Редактор статей'; // optional
        $role->save();

        $role = new Role();
        $role->name         = Role::ROLE_USER;
        $role->display_name = 'Пользователь'; // optional
        $role->description  = 'Просто пользователь. Просто чмо.'; // optional
        $role->save();

        $role = new Role();
        $role->name         = Role::ROLE_SUPERADMIN;
        $role->display_name = 'Суперадмин'; // optional
        $role->description  = 'Больше чем бог'; // optional
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::whereIn('name', [Role::ROLE_ADMIN, Role::ROLE_EDITOR, Role::ROLE_USER, Role::ROLE_SUPERADMIN])->delete();
    }
}
