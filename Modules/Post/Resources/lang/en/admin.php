<?php

return [
    'add' => 'Add',
    'save' => 'Save',
    'download' => 'Download',
    'cancel' => 'Cancel',
    'actions' => 'Actions',
    'author' => 'Author',
    'title' => 'Title',
    'type' => 'Type',
    'name' => 'Name',
    'content' => 'Content',
    'status' => 'Status',
    'width' => 'Width',
    'enter_title' => 'Enter title',
    'main_image' => 'Main image',
    'crop' => 'Crop',

    'post' => [
        'list' => 'List of posts',
        'title_type' => 'Title and type',
        'create' => 'Creating a post',
        'edit' => 'Editing a post',
        'select_type' => 'Select type',
        'select_status' => 'Select status'
    ],

    'types' => [
        'list' => 'Types of posts',
        'name' => 'Type name',
        'create' => 'Creating a post type',
        'edit' => 'Editing a post type',
    ]
];