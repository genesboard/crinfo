@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('posts') }}
@endsection
@section('content')
<div class="tray tray-center">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <span class="panel-icon"><i class="fa fa-list"></i></span>
                        <span class="panel-title">{{__('post::admin.post.list')}}</span>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a class="btn btn-success btn-xs" href="{{route('post.posts.create')}}">
                            <i class="fa fa-plus"></i>
                            {{__('post::admin.add')}}
                        </a>
                    </div>
                </div>
            </div>
            @isset($posts)
                <div class="panel-body pn">
                    <div class="table-responsive">
                        <table class="table admin-form table-condensed table-responsive table-striped table-bordered">
                            <thead>
                            <tr class="bg-light">
                                <th class="text-center">#</th>
                                <th class="text-center">{{__('post::admin.title')}}</th>
                                <th class="text-center">{{__('post::admin.type')}}</th>
                                <th class="text-center">{{__('post::admin.author')}}</th>
                                <th class="text-center">{{__('post::admin.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td class="text-center">{{$loop->index + 1}}</td>
                                    <td class="text-center">{{$post->title}}</td>
                                    <td class="text-center">
                                        @isset($post->postType)
                                            {{$post->postType->name}}
                                        @endisset
                                    </td>
                                    <td class="text-center">
                                        @isset($post->user)
                                            {{$post->user->name}} {{$post->user->lastname}}
                                        @endisset
                                    </td>
                                    <td class="text-right">
                                        {{ Form::open(['method' => 'delete', 'route' => ['post.posts.destroy', $post->id]]) }}
                                        <a href="{{route('post.posts.edit', $post->id)}}">
                                            {{Form::button('<i class="fa fa-edit"></i>', ['class' => 'btn btn-xs btn-warning'])}}
                                        </a>
                                        {{ Form::hidden('id', $post->id) }}
                                        <button type="submit" class="btn btn-xs btn-danger confirm" data-confirm="{{__('validation.confirm_delete')}}"><i class="fa fa-times"></i></button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            <nav>{{ $posts->links() }}</nav>
                        </div>
                    </div>
                </div>
            @endisset
        </div>
    </div>
</div>
@endsection
