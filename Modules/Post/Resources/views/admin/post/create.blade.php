@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('post-create') }}
@endsection
@section('content')
    <div class="admin-form">
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable col-sm-8 col-sm-offset-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="fa fa-warning pr10"></i>
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <div class="panel panel-dark heading-border col-sm-8 col-sm-offset-2">
            <div class="panel-heading">
                <span class="panel-title">{{ __('post::admin.post.create') }}</span>
            </div>
            {{Form::open(['route' => 'post.posts.store', 'files' => true, 'class' => 'horizontal'])}}
            <div class="panel-body">
                {{Form::hidden('user_id', Auth::id())}}
                <div class="section row mt30">
                    {{Form::label('title', __('post::admin.title'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label for="names" class="field prepend-icon">
                            {{Form::text('title', old('title'), ['id' => 'title', 'class' => 'gui-input', 'placeholder' => __('post::admin.enter_title')])}}
                            <label for="names" class="field-icon"><i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('type', __('post::admin.type'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label class="field select">
                            {{Form::select('type', $types, null, ['placeholder' => __('post::admin.post.select_type')])}}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>

                <div class="section row mt30">
                    {{Form::label('status', __('post::admin.status'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-6">
                        <label class="field select">
                            {{Form::select('status', $statuses, null, ['placeholder' => __('post::admin.post.select_status')])}}
                            <i class="arrow"></i>
                        </label>
                    </div>
                </div>

                @include('field::show')

                <div class="section mt40">
                    {{Form::label('content', __('post::admin.content'), ['class' => 'field-label'])}}
                    {{Form::textarea('content', null, ['class' => 'summernote'])}}
                </div>

                @include('admin.layouts.cropper', ['title' => __('post::admin.main_image'), 'fieldName' => 'main_image'])
                {{Form::submit(__('post::admin.save'), ['class' => 'btn btn-xs btn-success pull-right'])}}
            </div>

            {{Form::close()}}
        </div>
    </div>
@endsection
@section('add_script')
    {{--Cropper--}}
    <script src='https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js'></script>
@endsection