@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('post-type-create') }}
@endsection
@section('content')
    <div class="admin-form">
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable col-sm-8 col-sm-offset-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="fa fa-warning pr10"></i>
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <div class="panel panel-dark heading-border col-sm-8 col-sm-offset-2">
            <div class="panel-heading">
                <span class="panel-title">{{__('post::admin.types.create')}}</span>
            </div>
            {{Form::open(['route' => 'post.types.store', 'class' => 'horizontal'])}}
            <div class="panel-body">
                <div class="section row mt30">
                    {{Form::label('name', __('post::admin.types.name'), ['class' => 'col-sm-2 field-label'])}}
                    <div class="col-sm-10">
                        <label for="names" class="field prepend-icon">
                            {{Form::text('name', old('name'), ['id' => 'title', 'class' => 'gui-input', 'placeholder' =>  __('post::admin.name')])}}
                            <label for="names" class="field-icon"><i class="fa fa-user"></i>
                            </label>
                        </label>
                    </div>
                </div>
                {{Form::submit(__('post::admin.save'), ['class' => 'btn btn-xs btn-success pull-right'])}}
            </div>

            {{Form::close()}}
        </div>
    </div>
@endsection
