@extends('admin.layouts.app')
@section('crumb')
    {{ Breadcrumbs::render('post-types') }}
@endsection
@section('content')
<div class="tray tray-center">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-8">
                        <span class="panel-icon"><i class="fa fa-list"></i></span>
                        <span class="panel-title">{{__('post::admin.types.list')}}</span>
                    </div>
                    <div class="col-sm-4 text-right">
                        <a class="btn btn-success btn-xs" href="{{route('post.types.create')}}">
                            <i class="fa fa-plus"></i>
                            {{__('post::admin.add')}}
                        </a>
                    </div>
                </div>
            </div>
            @isset($types)
                <div class="panel-body pn">
                    <div class="table-responsive">
                        <table class="table admin-form table-condensed table-responsive table-striped table-bordered">
                            <thead>
                            <tr class="bg-light">
                                <th class="text-center">#</th>
                                <th class="text-center">{{__('post::admin.name')}}</th>
                                <th class="text-center">{{__('post::admin.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($types as $type)
                                <tr>
                                    <td class="text-center">{{$loop->index + 1}}</td>
                                    <td class="text-center">{{$type->name}}</td>
                                    <td class="text-right">
                                        {{ Form::open(['method' => 'delete', 'route' => ['post.types.destroy', $type->id]]) }}
                                        <a href="{{route('post.types.edit', $type->id)}}">
                                            {{Form::button('<i class="fa fa-edit"></i>', ['class' => 'btn btn-xs btn-warning'])}}
                                        </a>
                                        {{ Form::hidden('id', $type->id) }}
                                        <button type="submit" class="btn btn-xs btn-danger confirm" data-confirm="{{__('validation.confirm_delete')}}"><i class="fa fa-times"></i></button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            <nav>{{ $types->links() }}</nav>
                        </div>
                    </div>
                </div>
            @endisset
        </div>
    </div>
</div>
@endsection
