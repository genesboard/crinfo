<?php

namespace Modules\Post\Http\Requests;

use App\Helpers\Traits\RequestTrait;
use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    use RequestTrait;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'min:3|required',
            'type' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
