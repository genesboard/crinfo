<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'admin', 'namespace' => 'Modules\Post\Http\Controllers\Admin'], function()
{
    Route::get('/posts/list', 'PostController@index')->name('post.posts.list');
    Route::get('/posts/create', 'PostController@create')->name('post.posts.create');
    Route::post('/posts/store', 'PostController@store')->name('post.posts.store');
    Route::get('/posts/edit/{id}', 'PostController@edit')->name('post.posts.edit')->where('id', '[0-9]+');
    Route::put('/posts/update/{id}', 'PostController@update')->name('post.posts.update')->where('id', '[0-9]+');
    Route::delete('/posts/destroy/{id}', 'PostController@destroy')->name('post.posts.destroy')->where('id', '[0-9]+');

    Route::get('/post-types/list', 'PostTypeController@index')->name('post.types.list');
    Route::get('/post-types/create', 'PostTypeController@create')->name('post.types.create');
    Route::post('/post-types/store', 'PostTypeController@store')->name('post.types.store');
    Route::get('/post-types/edit/{id}', 'PostTypeController@edit')->name('post.types.edit')->where('id', '[0-9]+');
    Route::put('/post-types/update/{id}', 'PostTypeController@update')->name('post.types.update')->where('id', '[0-9]+');
    Route::delete('/post-types/destroy/{id}', 'PostTypeController@destroy')->name('post.types.destroy')->where('id', '[0-9]+');
});

Route::group(['middleware' => 'web', 'prefix' => 'post', 'namespace' => 'Modules\Post\Http\Controllers\Frontend'], function()
{
});
