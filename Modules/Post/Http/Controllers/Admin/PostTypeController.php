<?php

namespace Modules\Post\Http\Controllers\Admin;

use Modules\Post\Http\Requests\CreatePostTypeRequest;
use Modules\Post\Http\Requests\UpdatePostTypeRequest;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Services\PostTypeService;

class PostTypeController extends Controller
{
    protected $postTypeService;

    public function __construct(PostTypeService $postTypeService)
    {
        $this->postTypeService = $postTypeService;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('post::admin.type.index', [
            'types' => $this->postTypeService->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('post::admin.type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreatePostTypeRequest $request)
    {
        $this->postTypeService->create($request);
        return redirect(route('post.types.list'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return view('post::admin.type.edit', [
            'type' =>  $this->postTypeService->findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdatePostTypeRequest $request)
    {
        $this->postTypeService->update($request);
        return redirect(route('post.types.list'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $this->postTypeService->delete($id);
        return redirect(route('post.types.list'));
    }
}
