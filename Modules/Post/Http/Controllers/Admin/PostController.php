<?php

namespace Modules\Post\Http\Controllers\Admin;

use Modules\Post\Http\Requests\CreatePostRequest;
use Modules\Post\Http\Requests\UpdatePostRequest;
use Illuminate\Routing\Controller;
use Modules\Post\Services\PostService;
use Modules\Field\Services\FieldService;

class PostController extends Controller
{
    /**
     * @var FieldService
     */
    protected $fieldService;
    protected $postService;

    public function __construct(PostService $postService, FieldService $fieldService)
    {
        $this->postService = $postService;
        $this->fieldService = $fieldService;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('post::admin.post.index', [
            'posts' => $this->postService->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('post::admin.post.create', [
            'types' => $this->postService->getTypesArray(),
            'statuses' => $this->postService->getStatuses(),
            'fields' => $this->fieldService->getFields()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {
        $this->postService->create($request);
        return redirect(route('post.posts.list'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return view('post::admin.post.edit', [
            'post' =>  $this->postService->findOrFail($id),
            'types' => $this->postService->getTypesArray(),
            'fields' => $this->fieldService->getFields($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdatePostRequest $request)
    {
        $this->postService->update($request);
        return redirect(route('post.posts.list'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $this->postService->delete($id);
        return redirect(route('post.posts.list'));
    }
}
