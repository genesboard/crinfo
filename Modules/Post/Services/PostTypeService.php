<?php

namespace Modules\Post\Services;

use Illuminate\Http\Request;
use Modules\Post\Entities\PostType;

class PostTypeService
{
    const MODEL_NAME = 'post';

    public function paginate()
    {
        return PostType::paginate(config('constants.NUM_PAG'));
    }

    public function create(Request $request)
    {
        PostType::create($request->all());
    }

    public function findOrFail($id)
    {
        return PostType::findOrFail($id);
    }

    public function update(Request $request)
    {
        $type = PostType::findOrFail($request->id);
        $type->update($request->all());
    }

    public function delete($id)
    {
        PostType::findOrFail($id)->delete();
    }
}
