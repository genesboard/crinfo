<?php

namespace Modules\Post\Services;

use Illuminate\Http\Request;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\PostType;

class PostService
{
    const MODEL_NAME = 'post';

    public function paginate()
    {
        return Post::paginate(config('constants.NUM_PAG'));
    }

    public function getTypesArray()
    {
        return PostType::pluck('name', 'id')->toArray();
    }

    public function getStatuses()
    {
        return Post::getStatuses();
    }

    /**
     * @param Request $request
     */
    public static function create(Request $request)
    {
        $request->merge([
            'content' => saveImagesSummernote($request->get('content'), self::MODEL_NAME)
        ]);
        $post = Post::create($request->all());

        if ($request->field) {
            foreach ($request->field as $id => $field) {
                if ($field) {
                    $post->fields()->attach($id, ['value' => $field]);
                }
            }
        }
    }

    public function findOrFail($id)
    {
        return Post::findOrFail($id);
    }

    /**
     * @param Request $request
     */
    public static function update(Request $request)
    {
        $post = Post::findOrFail($request->id);
        $request->merge([
            'content' => saveImagesSummernote($request->get('content'), self::MODEL_NAME)
        ]);
        $post->update($request->all());

        $post->fields()->detach();

        if ($request->field) {
            foreach ($request->field as $id => $field) {
                if ($field) {
                    $post->fields()->attach($id, ['value' => $field]);
                }
            }
        }
    }

    public function delete($id)
    {
        $post = Post::findOrFail($id);
        $post->fields()->detach();
        $post->delete();
    }
}
