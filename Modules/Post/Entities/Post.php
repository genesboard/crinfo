<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use Modules\Post\Entities\PostType;
use Modules\Category\Entities\Category;
use Modules\Field\Entities\Field;

class Post extends Model
{
    const STATUS_DEACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $fillable = ['title', 'content', 'user_id', 'type', 'status', 'main_image', 'name'];

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }

    public function postType() {
        return $this->belongsTo(PostType::class, 'type');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public static function getStatuses() {
        return [
            self::STATUS_DEACTIVE => 'неактивный',
            self::STATUS_ACTIVE => 'активный'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function fields()
    {
        return $this->morphToMany(Field::class, 'fieldable')->withPivot('value');
    }
}
