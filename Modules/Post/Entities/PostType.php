<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Post\Entities\Post;

class PostType extends Model
{
    protected $fillable = ['name'];

    public function posts(){
        return $this->hasMany(Post::class,'type');
    }
}
