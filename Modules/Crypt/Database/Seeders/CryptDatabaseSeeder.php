<?php

namespace Modules\Crypt\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;

class CryptDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('stocks')->insert(['stock' => 'wex', 'stock_url' => 'https://wex.nz/']);
        DB::table('stocks')->insert(['stock' => 'exmo', 'stock_url' => 'https://exmo.me/']);
    }
}
