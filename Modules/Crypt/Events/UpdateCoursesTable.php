<?php

namespace Modules\Crypt\Events;

use Illuminate\Queue\SerializesModels;

class UpdateCoursesTable
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function updateCourses()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.coinmarketcap.com/v1/ticker/']);
        $response = $client->request('GET');
        $courses = collect(json_decode($response->getBody()));

        foreach ($courses as $course) {
            $crs = \Modules\Crypt\Entities\Course::findOrNew($course->rank);
            $crs->id_coin = $course->id;
            $crs->name_coin = $course->name;
            $crs->symbol = $course->symbol;
            $crs->price_usd = $course->price_usd;
            $crs->save();
        }
    }
}
