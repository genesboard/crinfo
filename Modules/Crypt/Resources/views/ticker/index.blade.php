@extends('admin.layouts.table')

@section('content_table')

    @section('title', 'Ticker')
    @section('routeButton', route('admin.ticker.create'))
    @section('titleButton', 'Add')

    @isset($ticker)

    <div class="col-sm-6 col-lg-4">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">Wex</span>
                    <div class="widget-menu pull-right mr10">
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-primary">
                                <span class="glyphicon glyphicon-trash fs11 mr5"></span></button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-6">
                        <h1><span class="label label-primary">{{ round($ticker->btc_usd->last) }} $</span></h1>
                        <br>
                        <span class="btn btn-lg btn-success">
                            24h Volume:
                            <br>{{ round($ticker->btc_usd->vol) }}
                        </span>
                    </div>
                    <div class="col-sm-6 list-group">
                        <span class="list-group-item">
                            <i class="fa fa-chevron-circle-up fa-fw" aria-hidden="true"></i>
                            HIGH: {{ round($ticker->btc_usd->high) }} $
                        </span>
                        <span class="list-group-item">
                            <i class="fa fa-chevron-circle-down fa-fw" aria-hidden="true"></i>
                            LOW: {{ round($ticker->btc_usd->low) }} $
                        </span>
                        <span class="list-group-item">
                            <i class="fa fa-money fa-fw" aria-hidden="true"></i>
                            ASK: {{ round($ticker->btc_usd->buy) }} $
                        </span>
                        <span class="list-group-item">
                            <i class="fa fa-money fa-fw" aria-hidden="true"></i>
                            BID: {{ round($ticker->btc_usd->sell) }} $
                        </span>
                    </div>
                </div>
            </div>
    </div>





<div class="col-sm-12">
    <pre>
    <a href="https://wex.nz/api/3/info" target="_blank">https://wex.nz/api/3/info</a>
    <a href="https://wex.nz/api/3/ticker/btc_usd-btc_rur" target="_blank">https://wex.nz/api/3/ticker/btc_usd-btc_rur</a>

    <a href="https://api.exmo.com/v1/ticker/" target="_blank">https://api.exmo.com/v1/ticker/</a>
    <a href="https://api.exmo.com/v1/currency/" target="_blank">https://api.exmo.com/v1/currency/</a>

    <a href="https://api.livecoin.net/exchange/ticker?currencyPair=BTC/USD" target="_blank">https://api.livecoin.net/exchange/ticker?currencyPair=BTC/USD</a>
    <a href="https://www.livecoin.net/api/public" target="_blank">https://www.livecoin.net/api/public</a>

    <a href="https://api.bitfinex.com/v1/pubticker/btcusd" target="_blank">https://api.bitfinex.com/v1/pubticker/btcusd</a>
    <a href="https://api.bitfinex.com/v1/symbols" target="_blank">https://api.bitfinex.com/v1/symbols</a>

    <a href="https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc" target="_blank">https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc</a>
    <a href="https://bittrex.com/api/v1.1/public/getcurrencies" target="_blank">https://bittrex.com/api/v1.1/public/getcurrencies</a>

    <a href="https://www.bitstamp.net/api/v2/ticker/xrpusd" target="_blank">https://www.bitstamp.net/api/v2/ticker/xrpusd</a>
    <a href="https://www.bitstamp.net/api/" target="_blank">https://www.bitstamp.net/api/</a>

    <a href="https://yobit.net/api/3/ticker/ltc_btc" target="_blank">https://yobit.net/api/3/ticker/ltc_btc</a>
    <a href="https://yobit.net/api/3/info" target="_blank">https://yobit.net/api/3/info</a>

    <a href="https://api.kraken.com/0/public/Assets" target="_blank">https://api.kraken.com/0/public/Assets</a>
    <a href="https://api.kraken.com/0/public/Ticker?pair=XETHZEUR" target="_blank">https://api.kraken.com/0/public/Ticker?pair=XETHZEUR</a>
    </pre>
</div>

    @endisset

@endsection
