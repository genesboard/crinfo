@extends('admin.layouts.form')

@section('content_form')
    {{ Form::model($portfolio, ['route' => 'portfolio.store', 'files' => true, 'class' => 'form-horizontal']) }}

        @component('admin.components.form.devider'){{ __('index.main_data') }}@endcomponent

        <div class="form-group">
            {{ Form::adminText('coin_val', 'Количество', 3, 3) }}
            {{ Form::adminSelect('coin', 'Монета', $portfolio->getCoins(), 2, 3) }}
        </div>

        <div class="form-group">
            {{ Form::adminText('cost_val', 'Курс покупки', 3, 3) }}
            {{ Form::adminSelect('fiat', 'Фиат', $portfolio->getFiats(), 2, 3) }}
        </div>

        <div class="form-group">
            {{ Form::adminSelect('stock_id', 'Биржа', $stocks, 3, 8) }}
        </div>

        @if ($portfolio->exists)
            {{ Form::hidden('id', $portfolio->id) }}
        @endif

        {{ Form::hidden('user_id', Auth::id()) }}

        @component('admin.components.form.devider'){{ __('index.actions') }}@endcomponent

    {{ Form::adminSubmit() }}

    {{Form::close()}}
@endsection