@extends('admin.layouts.table')

@section('content_table')

    @section('title', 'Portfolio')
    @section('routeButton', route('portfolio.create'))
    @section('titleButton', 'Add Coin')

    @if ($portfolio->count())
    <table class="table table-bordered table-responsive table-striped">
        <thead>
        <tr class="bg-light">
            <th>Монета</th>
            <th>Количество</th>
            <th>Курс (покупки/текущий)</th>
            <th>Сумма (затрат/текущая)</th>
            <th>Прибыль/Убыток</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        @forelse($portfolio as $p)
            <tr>
                <td>
                    <span class="label label-default">{{ $p->coin }}</span>
                </td>
                <td>
                    {{ rtrim($p->coin_val, '0') }}
                    <a href="{{ $p->stock->stock_url }}" target="_blank"
                       class="label label-primary pull-right"
                       style="padding: .1em .4em; font-size: 75%; line-height: 15px;">
                        {{ $p->stock->stock }}
                    </a>
                </td>
                <td>{{ $p->rate_old }} / {{ $p->rate_now }}</td>
                <td>{{ $p->cost_old }} / {{ $p->cost_now }}</td>
                <td class="text-{{ ($p->gain_lost > 0) ? 'primary' : 'danger'}}">{{ $p->gain_lost }} {{ $p->fiat }}</td>
                <td>
                    {{ Form::adminActions(
                        route('portfolio.edit', ['aff_system' => $p->id]),
                        route('portfolio.destroy', ['portfolio' => $p])
                    ) }}
                </td>
            </tr>
        @empty
            <tr><td colspan="6">{{ __('index.data_not_found') }}</td></tr>
        @endforelse
            <tr>
                <td colspan="2" class="text-right">Депозит: {{ $deposit }} {{ $p->fiat }}</td>
                <td colspan="1" class="text-right text-{{ ($balance > 0) ? 'primary' : 'danger'}}">Баланс: {{ $balance }} {{ $p->fiat }}</td>
                <td colspan="3" class="text-right">Итого: {{ $deposit + $balance }} {{ $p->fiat }}</td>
            </tr>
        </tbody>
    </table>

    <div>
        {{ $portfolio->links() }}
    </div>
    @else
        <span>Пришло время добавить валюту!</span>
    @endif

@endsection