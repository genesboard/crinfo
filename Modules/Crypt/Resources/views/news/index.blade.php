@extends('admin.layouts.app')

<style>
    .single-blog {
        display:inline-block;
        position:relative;
        margin-bottom: 50px;
        -webkit-box-shadow: 0 0 2px 0 rgba(51, 51, 51, 0.08), 0 0 2px 0 rgba(51, 51, 51, 0.08);
        box-shadow: 0 0 2px 0 rgba(51, 51, 51, 0.08), 0 0 2px 0 rgba(51, 51, 51, 0.08);
    }

    .single-blog .post-content {
        overflow: hidden;
        padding: 40px;
    }

    .single-blog .post-content .entry-header .post-cat
    {
        color: #da521e;
        display: inline-block;
        font-size: 11px;
        font-weight: 700;
        letter-spacing: 1px;
        margin-bottom: 10px;
    }

    img {
        max-width: 100%;
    }

    .single-blog .post-content .continue-reading a {
        border: 1px solid #da521e;
        color: #da521e;
        display: inline-block;
        font-size: 12px;
        font-weight: 700;
        letter-spacing: 1px;
        padding: 10px 18px;
    }

    .single-blog .post-content .post-meta {
        overflow: hidden;
        border-top: 1px solid #e2e2e2;
        margin-top: 40px;
        padding-top: 20px;
        line-height: 1;
        margin-bottom: 0;
        padding-bottom: 0;
    }

    .single-blog .post-content {
        display:block;
        position:absolute;
        left:0;
        bottom:0;
        width:100%;
        box-sizing:border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding:10px;
        background-color:rgba(255,255,255,.8);
    }
</style>

@section('content')
    @isset($news)
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-1 col-sm-10">

            @forelse($news as $n)
            <article class="single-blog">
                <a href="{{ $n->url }}" target="_blank">
                    <img src="{{ $n->urlToImage }}">
                </a>
                <div class="post-content">
                    <div class="entry-header text-center text-uppercase">
                        <h5 class="post-cat">{{ $n->source->name }}</h5>
                        <h2>{{ $n->title }}</h2>
                    </div>
                    <div class="entry-content">
                        <blockquote>
                            <p>{{ $n->description }}</p>
                        </blockquote>
                    </div>
                    {{--
                    <div class="continue-reading text-center text-uppercase">
                        <a href="{{ $n->url }}" target="_blank">Подробней</a>
                    </div>
                    --}}
                    <div class="post-meta hidden">
                        <ul class="pull-left list-inline author-meta">
                            @isset($n->author)
                                <li class="author">{{ $n->author }}</li>
                            @endisset
                        </ul>
                        <ul class="pull-right list-inline social-share">
                            <li class="date">{{ $n->publishedAt }}</li>
                        </ul>
                    </div>

                </div>
            </article>
            @empty
                <h1>{{ __('index.data_not_found') }}</h1>
            @endforelse

            </div>
        </div>
    </div>
    @endisset
@endsection
