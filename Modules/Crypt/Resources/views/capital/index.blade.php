@extends('admin.layouts.app')

@section('content')

    @isset($capital)
    <div class="panel">
        <div class="panel-heading panel-visible">
            <span class="panel-title">Cryptocurrency Market Capitalizations</span>
        </div>

        <div class="panel-body">
            <div class="bs-component">
                <table class="table table-bordered table-responsive table-striped">
                    <thead>
                    <tr class="bg-light">
                        <th>Symbol</th>
                        <th>Coin</th>
                        <th>Price</th>
                        <th>Market Cap</th>
                        <th>Circulating Supply</th>
                        <th>% 1h</th>
                        <th>% 24h</th>
                        <th>% 7d</th>
                    </tr>
                    </thead>
                    <tbody>

                    @forelse($capital as $t)
                    <tr class="text-center">
                        <td>
                            <span class="label label-default">{{ $t->symbol }}</span>
                        </td>
                        <td>
                            <b>{{ $t->name }}</b>
                        </td>
                        <td>
                            {{ moneyFormat($t->price_usd) }}
                            {{ indexArrowInt($t->percent_change_1h) }}
                        </td>
                        <td>{{ moneyFormat($t->market_cap_usd, 0) }}</td>
                        <td>{{ moneyFormat($t->total_supply, 0) }}</td>
                        <td class="text-{{ ($t->percent_change_1h > 0) ? 'primary' : 'danger'}}">{{ $t->percent_change_1h }}</td>
                        <td class="text-{{ ($t->percent_change_24h > 0) ? 'primary' : 'danger'}}">{{ $t->percent_change_24h }}</td>
                        <td class="text-{{ ($t->percent_change_7d > 0) ? 'primary' : 'danger'}}">{{ $t->percent_change_7d }}</td>
                    </tr>
                    @empty
                    <tr><td colspan="7">{{ __('index.data_not_found') }}</td></tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endisset

@endsection
