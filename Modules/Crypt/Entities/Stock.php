<?php

namespace Modules\Crypt\Entities;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['stock', 'stock_url'];
}
