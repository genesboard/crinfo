<?php

namespace Modules\Crypt\Entities;

use App\Models\Base;

class Course extends Base
{
    protected $fillable = [
        'id_coin', 'name', 'symbol', 'price_usd'
    ];
}
