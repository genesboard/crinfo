<?php

namespace Modules\Crypt\Entities;

use App\Models\Base;
use App\Models\User\User;

class Portfolio extends Base
{
    const FIAT_RUB = 'RUB';
    const FIAT_USD = 'USD';

    protected $fillable = [
        'coin', 'coin_val', 'fiat', 'cost_val', 'user_id', 'stock_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function course()
    {
        return $this->hasOne(Course::class, 'symbol', 'coin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stock()
    {
        return $this->hasOne(Stock::class, 'id', 'stock_id');
    }

    /**
     * @return float
     */
    public function getDeposit()
    {
        $data = Portfolio::where('user_id', Auth::user()->id)
            ->selectRaw('(coin_val * cost_val) as result')
            ->get()
            ->sum('result');

        return round($data, 2);
    }
    
    /**
     * @param null $fiat
     * @return array|mixed|string
     */
    public function getFiats($fiat = null)
    {
        $data = [
            self::FIAT_USD => self::FIAT_USD,
            self::FIAT_RUB => self::FIAT_RUB,
        ];

        return $fiat !== null ? ($data[$fiat] ?? '') : $data;
    }

    /**
     * @param null $coin
     * @return string
     */
    public function getCoins($coin = null)
    {
        $url = 'https://api.coinmarketcap.com/v1/ticker/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($ch));

        foreach ($result as $r) {
            $data[$r->symbol] = $r->name;
        }

        return $coin !== null ? ($data[$coin] ?? '') : $data;
    }

    /**
     * @return string
     */
    public function getRateOldAttribute()
    {
        return $this->cost_val . ' ' . $this->fiat;
    }

    /**
     * @return string
     */
    public function getRateNowAttribute()
    {
        return $this->course->price_usd . ' ' . $this->fiat;
    }

    /**
     * @return string
     */
    public function getCostOldAttribute()
    {
        return round($this->cost_val * $this->coin_val, 2) . ' ' . $this->fiat;
    }

    /**
     * @return string
     */
    public function getCostNowAttribute()
    {
        return round($this->course->price_usd * $this->coin_val, 2) . ' ' . $this->fiat;
    }

    /**
     * @return string
     */
    public function getGainLostAttribute()
    {
        return round($this->coin_val * ($this->course->price_usd - $this->cost_val), 2);
    }
}