<?php

namespace Modules\Crypt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TickerController extends Controller
{
    public function index()
    {
        return view('crypt::ticker.index', [
            'ticker' => $this->getTickerWex(),
        ]);
    }

    public function getTickerWex()
    {
        $url = 'https://wex.nz/api/3/ticker/btc_usd-btc_rur';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($ch));
        return $result;
    }
}
