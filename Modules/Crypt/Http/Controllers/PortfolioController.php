<?php

namespace Modules\Crypt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Crypt\Entities\Portfolio;
use Auth;
use Modules\Crypt\Entities\Stock;
use Event;

class PortfolioController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Event::fire('courses.update');

        return view('crypt::portfolio.index', [
            'portfolio' => Portfolio::where('user_id', Auth::user()->id)
                ->with('user', 'course', 'stock')
                ->paginate(20),
            'deposit' => Auth::user()->getDeposit(),
            'balance' => Auth::user()->getGainLost()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->getForm();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Portfolio::upsert($request->input());

        return redirect(route('portfolio.index'));
    }

    /**
     * @param Portfolio $portfolio
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Portfolio $portfolio)
    {
        return $this->getForm($portfolio->id);
    }

    /**
     * @param Portfolio $portfolio
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Portfolio $portfolio)
    {
        Portfolio::destroy($portfolio->id);

        return redirect(route('portfolio.index'));
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getForm($id = null)
    {
        $portfolio = $id ? Portfolio::find($id) : new Portfolio();

        return view('crypt::portfolio.form', [
            'portfolio' => $portfolio,
            'stocks' => $this->getStocks()
        ]);
    }

    /**
     * @return array
     */
    public function getStocks()
    {
        return Stock::pluck('stock', 'id')->toArray();
    }
}
