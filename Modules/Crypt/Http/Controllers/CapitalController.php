<?php

namespace Modules\Crypt\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CapitalController extends Controller
{
    public function index()
    {
        return view('crypt::capital.index', [
            'capital' => $this->getCapital(),
        ]);
    }

    public function getCapital()
    {
        $url = 'https://api.coinmarketcap.com/v1/ticker/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        return $result = json_decode(curl_exec($ch));
    }
}
