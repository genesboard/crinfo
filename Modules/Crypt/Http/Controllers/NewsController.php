<?php

namespace Modules\Crypt\Http\Controllers;

use Illuminate\Routing\Controller;

class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('crypt::news.index', [
            'news' => $this->getNews()
        ]);
    }

    /**
     * @return array
     */
    public function getNews()
    {
        $url = 'https://newsapi.org/v2/top-headlines?sources=crypto-coins-news&apiKey=134fdc34cfb04d6ea5a4cc631d73b4cf';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = json_decode(curl_exec($ch));

        return $result->articles;
    }
}
