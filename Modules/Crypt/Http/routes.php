<?php

Route::group(['middleware' => ['web', 'auth'], 'namespace' => 'Modules\Crypt\Http\Controllers'], function()
{
    Route::resource('portfolio', 'PortfolioController');
    Route::resource('capital', 'CapitalController');
    Route::resource('news', 'NewsController');

    Route::resource('ticker', 'TickerController');
});